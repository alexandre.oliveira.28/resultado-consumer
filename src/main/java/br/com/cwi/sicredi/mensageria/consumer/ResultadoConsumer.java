package br.com.cwi.sicredi.mensageria.consumer;

import java.text.DecimalFormat;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import br.com.cwi.sicredi.mensageria.constantes.Constantes;
import br.com.cwi.sicredi.mensageria.dto.ResultadoDTO;

@Component
public class ResultadoConsumer {
	
	@RabbitListener(queues = Constantes.FILA_RESULTADO_VOTACAO)
	private void consumidor(ResultadoDTO resultadoDto) {
		
		if(resultadoDto.votacaoEncerrada) {
			Double totalVotos = (double) (resultadoDto.valorNao + resultadoDto.valorSim); 
			Double percentualSim = (double) ((resultadoDto.valorSim * 100) / totalVotos);
			Double percentualNao = (double) ((resultadoDto.valorNao * 100) / totalVotos);
			
			DecimalFormat formatador = new DecimalFormat("0.00");
			
			System.out.println("----=== Resultado Final da Votação após seu encerramento ===----");
			System.out.println("Assembleia Geral de Nº " + resultadoDto.idAssembleia);
			System.out.println("Votos SIM: " + resultadoDto.valorSim + " - " + formatador.format(percentualSim) + "%");
			System.out.println("Votos NÃO: " + resultadoDto.valorNao + " - " + formatador.format(percentualNao) + "%");
		}else {
			System.out.println("----=== Aguarde encerramento da votação para ver o Resultado ===----");
		}
	}

}
